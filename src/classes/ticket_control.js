const fs = require('fs')

class Ticket {
    constructor(numero, escritorio) {
        this.numero = numero
        this.escritorio = escritorio
    }
}

class TicketControl {

    constructor() {
        this.ultimo = 0;
        this.hoy = new Date().getDate();
        this.tickets = [];
        this.ultimos4 = [];

        let data = require('../data/data')
        if (data.hoy === this.hoy) {
            this.ultimo = data.ultimo
            this.tickets = data.tickets
            this.ultimos4 = data.ultimos4;

        } else {
            this.reiniciarConteo()
        }
    }

    siguienteTicker() {
        this.ultimo++;
        let ticket = new Ticket(this.ultimo, null)
        this.tickets.push(ticket)

        this.guardarArchivo()
        return `Ticket ${this.ultimo}`
    }
    getUltimoTicket() {
        return `Ticket ${this.ultimo}`
    }
    atenderTicket(escritorio) {
        if (this.tickets.length === 0) {
            return "ya no hay ticket por atender"
        }
        let numeroTicket = this.tickets[0].numero
        this.tickets.shift() //borra el primer elemento

        let atenderTicket = new Ticket(numeroTicket, escritorio);
        this.ultimos4.unshift(atenderTicket) //agregar ticket al inicio

        if (this.ultimos4.length > 4) {
            this.ultimos4.splice(-1, 1)
        }
        console.log(this.ultimos4);
        this.guardarArchivo();

        return atenderTicket;
    }
    getultimos4() {
        return this.ultimos4;
    }

    guardarArchivo() {
        let jsonData = {
            ultimo: this.ultimo,
            hoy: this.hoy,
            tickets: this.tickets,
            ultimos4: this.ultimos4
        }
        let dataString = JSON.stringify(jsonData);

        fs.writeFileSync('./src/data/data.json', dataString);
    }
    reiniciarConteo() {
        this.ultimo = 0;
        this.tickets = [];
        this.ultimos4 = [];

        this.guardarArchivo()

    }

}

module.exports = { TicketControl }