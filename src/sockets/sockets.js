
const { io } = require('../app')
const { TicketControl } = require('../classes/ticket_control')


const ticketControl = new TicketControl();
io.on('connection', (client) => {


    client.on('atenderTicket', (data, callback) => {

        if (!data.escritorio) {
            return callback({ err: true, mensaje: 'El escritorio es necesario' })
        }

        let atenderTicket = ticketControl.atenderTicket(data.escritorio)

        callback(atenderTicket)

        client.broadcast.emit('ultimos4',{ultimos4:ticketControl.getultimos4()})


    })

    client.on('disconnect', () => {
        console.log('usuario desconectado');
    })

    client.on('siguienteTicket', (data, callback) => {
        let siguienteTicker = ticketControl.siguienteTicker();
        callback(siguienteTicker)

    });

    client.emit('estadoActual', { actual: ticketControl.getUltimoTicket(),ultimos4: ticketControl.getultimos4() })



});

