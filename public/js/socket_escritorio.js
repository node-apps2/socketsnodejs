var socket = io();


var searchParams = new URLSearchParams(window.location.search)

if (!searchParams.has('escritorio')) {
    window.location = 'index.html';
    throw new Error('el escritorio es requeriodo')
}

let escritorio = searchParams.get('escritorio');
var label = $('small')
$('h1').text(`Escritorio ${escritorio}`);

$("button").click(function (e) {

    socket.emit('atenderTicket', { escritorio: escritorio }, function (ticket) {
        if (ticket === 'ya no hay ticket por atender') {
            label.text(ticket)
            return;
        }
        label.text(`Ticket ${ticket.numero}`)
    })
});


// console.log(escritorio);