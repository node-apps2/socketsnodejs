var socket = io();
var label = $("#lblNuevoTicket")
// socket.on('connect', function () {
//     console.log('conectado al servidor');
// });

// socket.on('disconnect', function () {
//     console.log('perdimos conexion con el servidor');
// })

socket.on('estadoActual', function (data) {
    label.text(data.actual);
})

$("#nuevo-ticket").click(function (e) {
    e.preventDefault();

    socket.emit('siguienteTicket', null, function (siguienteTicket) {

        label.text(siguienteTicket);
    });

});