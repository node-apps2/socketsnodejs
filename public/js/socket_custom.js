var socket = io();
socket.on('connect', function () {
    console.log('conectado al servidor');
});
socket.on('enviarMensaje', function (data) {
    console.log('data del servidor', data);
})


socket.on('disconnect', function () {
    console.log('perdimos conexion con el servidor');
})


//lo emit son para enviar para informacion
socket.emit('enviarMensaje', {
    usuario: 'rafael',
    mensaje: 'hola mundo'
}, function (response) {
    console.log("respuesta server" ,response)
});
